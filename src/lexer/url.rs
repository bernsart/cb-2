use logos::{Lexer, Logos, Filter };
use std::fmt::{Display, Formatter};
use regex::Regex;

/// Tuple struct for link URLs
#[derive(Debug, PartialEq)]
pub struct LinkUrl(String);

/// Implement Display for printing
impl Display for LinkUrl {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Tuple struct for link texts
#[derive(Debug, PartialEq)]
pub struct LinkText(String);

/// Implement Display for printing
impl Display for LinkText {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Token enum for capturing of link URLs and Texts
#[derive(Logos, Debug, PartialEq)]
pub enum URLToken {
    // TODO: Capture link definitions
    #[regex(r#"<a[^>h]*[^>]*>[^<]*</a[\s]*>"#, |lex| {
        let re = Regex::new(r#"href=""#).unwrap();
        if re.is_match(lex.slice()){
        Filter::Emit(extract_link_info(lex))}
        else{
            Filter::Skip
        }
    };)]
    Link((LinkUrl, LinkText)),

    //TODO: Ignore all characters that do not belong to a link definition
    #[regex(r"[\s\S]",logos::skip)]
    Ignored,

    // Catch any error
    #[error]
    
    Error,
}

/// Extracts the URL and text from a string that matched a Link token
fn extract_link_info(lex: &mut Lexer<URLToken>) -> (LinkUrl, LinkText) {
    //get url text
    let text1= lex.slice();
    let re1 = Regex::new("href=\"[^\"]*\"").unwrap();
    let text2 =  match re1.captures(&text1){Some(caps) => caps[0].to_string(),None => String::from("")};
    let re2 = Regex::new("\"[^\"]*\"").unwrap();
    let text3 = match re2.captures(&text2){Some(caps) => caps[0].to_string(),None => String::from("")};
    let re3 = Regex::new("[^\"]+").unwrap();
    let url_text = match re3.captures(&text3){Some(caps) => caps[0].to_string(),None => String::from("")};
    //get link Text
    let re4 = Regex::new(">[^<]*<").unwrap();
    let text4 =  match re4.captures(&text1){Some(caps) => caps[0].to_string(),None => String::from("")};
    let re5 = Regex::new("[^<>]+").unwrap();
    let link_text = match re5.captures(&text4){Some(caps) => caps[0].to_string(),None => String::from("")};
    (LinkUrl(url_text),LinkText(link_text))
}
